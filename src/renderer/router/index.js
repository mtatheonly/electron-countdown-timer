import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

const LandingPage = () => import('@/components/LandingPage.vue')
const MediaSelection = () => import('@/components/MediaSelection.vue')
const Config = () => import('@/components/Config.vue')
const Preview = () => import('@/components/Preview.vue')
const Live = () => import('@/components/Live.vue')

export default new Router({
  routes: [
    {
      path: '/',
      name: 'landing-page',
      component: LandingPage
    },
    {
      path: '/media',
      name: 'media',
      component: MediaSelection
    },
    {
      path: '/config',
      name: 'config',
      component: Config
    },
    {
      path: '/preview',
      name: 'preview',
      component: Preview
    },
    {
      path: '/live',
      name: 'live',
      component: Live
    },
    {
      path: '*',
      redirect: '/'
    }
  ]
})
