const namespaced = true

const state = {
  photos: [],
  paths: []
}

const mutations = {
  addPhoto (state, payload) {
    state.photos.push(payload.photo)
    state.paths.push(payload.path)
  },
  removePhoto (state, index) {
    state.photos.splice(index, 1)
    state.paths.splice(index, 1)
  }
}

const actions = {
  someAsyncTask ({ commit }) {
    // do something async
    commit('INCREMENT_MAIN_COUNTER')
  }
}

export default {
  namespaced,
  state,
  mutations,
  actions
}
