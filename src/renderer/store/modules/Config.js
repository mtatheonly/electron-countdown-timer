const namespaced = true

const state = {
  dueTime: null,
  dueDate: null,
  loop: true,
  dateFormatMoment: 'DD-MM-YYYY HH:mm',
  dateFormatFlatpickr: 'd-m-Y H:i'
}

const mutations = {
  setDueTime (state, time) {
    state.dueTime = time
  },
  setLoop (state, loop) {
    state.loop = loop
  },
  setDueDate (state, date) {
    state.dueDate = date
  }
}

export default {
  namespaced,
  state,
  mutations
}
